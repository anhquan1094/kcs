/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.model.PhieuKCS;
import can.util.Dialog;
import can.util.PhieuCanPrinter;
import can.util.State;
import com.jfoenix.controls.JFXButton;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author maxcd
 */
public class ViewPhieuCanController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public Label tenCty;
    @FXML
    public Label chiNhanh;
    @FXML
    public Label sdt;
    @FXML
    public Label fax;
    @FXML
    public Label soPhieu;
    @FXML
    public Label tlTong;
    @FXML
    public Label tlMun;
    @FXML
    public Label tlQuaCo;
    @FXML
    public Label tlVo;
    @FXML
    public Label tlTapChat;
    @FXML
    public Label tyLeMun;
    @FXML
    public Label tyLeQuaCo;
    @FXML
    public Label tyLeVo;
    @FXML
    public Label tyLeTapChat;
    @FXML
    public Label doKho;
    @FXML
    public Label lb_loaihang;
    @FXML
    public Label lb_kieuSanPham;
    
    private PhieuKCS phieuCan;
    
    @FXML
    public void actionIn() {
        PrinterJob job = PrinterJob.getPrinterJob();
        PhieuCanPrinter printer = new PhieuCanPrinter(phieuCan);
        job.setPrintable(printer);
        boolean ok = job.printDialog();
        if (ok) {
            try {
                job.print();
                Dialog.showInformation("Đang in...");
            } catch (PrinterException ex) {
                ex.printStackTrace();
                Dialog.showError("Lỗi trong quá trình in!");
            }
        }
    }
    
    @FXML
    public void actionClose(ActionEvent event) {
        Stage stage = (Stage) doKho.getScene().getWindow();
        stage.close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        phieuCan = State.getPhieuCan();
        tenCty.setText(State.getCaiDat().tenCty);
        
        chiNhanh.setText("Chi nhánh: " + State.getCaiDat().tenChiNhanh);
        
        sdt.setText("SDT: " + State.getCaiDat().dtCty);
        
        fax.setText("FAX: " + State.getCaiDat().faxCty);
        
        soPhieu.setText("Số phiếu: " + phieuCan.soPhieu);
        
        tlTong.setText(phieuCan.tongTL + " gram");
        
        tlMun.setText(phieuCan.tlMun + " gram");
        
        tlQuaCo.setText(phieuCan.tlQuaCo + " gram");
        
        tlVo.setText(phieuCan.tlVo + " gram");
        
        tlTapChat.setText(phieuCan.tlTapChat + " gram");
        
        tyLeMun.setText(phieuCan.tyLeMun + "%");
        
        tyLeQuaCo.setText(phieuCan.tyLeQuaCo + "%");
        
        tyLeVo.setText(phieuCan.tyLeVo + "%");
        
        tyLeTapChat.setText(phieuCan.tyLeTapChat + "%");
        
        doKho.setText(phieuCan.doKho + "%");
    }
    
}
