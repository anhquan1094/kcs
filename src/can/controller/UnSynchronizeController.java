/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.model.PhieuKCS;
import can.util.State;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author maxcd
 */
public class UnSynchronizeController implements Initializable {

    @FXML
    private Button btn_close;
    @FXML
    private Button btn_export;
    @FXML
    public Label lb_infor;


    @FXML
    public void onClose() {
        Stage stage = (Stage) btn_close.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void onExport() {
        
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
        String sql2 = "";
        if (can.util.State.getUser().getRole() == 2) {
            sql2 = "SELECT * FROM PhieuKCS where sync=0;";
        } else if (can.util.State.getUser().getRole() == 3) {
            sql2 = "SELECT * FROM PhieuKCS where sync=0 and user_id=" + can.util.State.getUser().getId() + ";";
        }

        List<PhieuKCS> phieuCans = can.util.State.getEm().createNativeQuery(sql2, PhieuKCS.class).getResultList();

        int pc = phieuCans.size();
        String infor = "";
  
        if (pc > 0) {
            infor = "Bạn còn " + pc + " phiếu KCS chưa đồng bộ!";
        }
        lb_infor.setText(infor);
    }

}
