/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.Can;
import can.api.PushPhieuKCS;
import can.model.PhieuKCS;
import can.util.CheckInternet;
import can.util.Dialog;
import can.util.State;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author maxcd
 */
public class MenuViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public void onSync() {
        if (State.getUser().getRole() == 1) {
            Dialog.showWarning("Bạn không được quyền sử dụng chức năng này!");
            return;
        }
        if (CheckInternet.netIsAvailable()) {
            new PushPhieuKCS().run();
        }

        String sql2 = "";
        if (can.util.State.getUser().getRole() == 2) {
            sql2 = "SELECT * FROM PhieuKCS where sync=0;";
        } else if (can.util.State.getUser().getRole() == 3) {
            sql2 = "SELECT * FROM PhieuKCS where sync=0 and user_id=" + can.util.State.getUser().getId() + ";";
        }
        List<PhieuKCS> phieuCans = can.util.State.getEm().createNativeQuery(sql2, PhieuKCS.class).getResultList();
       
        int pcs = phieuCans.size();
        if (pcs == 0) {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/can/view/Synchronize.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Đồng bộ");
                stage.setScene(new Scene(root));
                stage.setResizable(false);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/can/view/UnSynchronize.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Đồng bộ");
                stage.setScene(new Scene(root));
                stage.setResizable(false);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @FXML
    public void showStatistics(ActionEvent event) {
        if (State.getUser().getRole() == 1) {
            Dialog.showWarning("Bạn không được quyền sử dụng chức năng này!");
            return;
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/can/view/Statistic.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Thống kê");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onDoiMatKhau() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/can/view/DoiMatKhau.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Đổi mật khẩu");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onQuanlyTaiKhoan() {
        if (State.getUser().getRole() == 3) {
            Dialog.showWarning("Bạn không được quyền sử dụng chức năng này!");
            return;
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/can/view/QuanlyTaiKhoan.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Quản lý tài khoản");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onCaiDat() {
        if (State.getUser().getRole() != 1) {
            Dialog.showWarning("Bạn không được quyền sử dụng chức năng này!");
            return;
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/can/view/CaiDat.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Cài đặt chung");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
