/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.dao.PhieuKCSJpaController;
import can.model.CaiDat;
import can.model.PhieuKCS;
import can.util.CheckInternet;
import can.util.ConvertExlToPdf;
import can.util.Dialog;
import can.util.State;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.Window;
import javax.persistence.EntityManager;
import jxl.CellView;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.joda.time.MutableDateTime;
import can.util.CellStyleUtil;

/**
 *
 * @author VMT
 */
public class StatisticsController implements Initializable {

    @FXML
    public DatePicker dp_startDate;
    @FXML
    public DatePicker dp_endDate;

    @FXML
    public Button btn_thongKe;
    @FXML
    public JFXButton btn_xem;
    @FXML
    public JFXButton btn_suaPhieu;
    @FXML
    public JFXButton btn_excel;
    @FXML
    public JFXButton btn_pdf;
    @FXML
    public TextField tf_maPhieu;
    @FXML
    public TableView<PhieuKCS> tbv_content;
    @FXML
    public TableColumn<PhieuKCS, String> cl_soPhieu;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tongTL;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlMun;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlQuaCo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlVo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlTapChat;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeMun;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeQuaCo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeVo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeTapChat;
    @FXML
    public TableColumn<PhieuKCS, String> cl_doKho;
    @FXML
    public TableColumn<PhieuKCS, String> cl_thoiGian;
    private List<PhieuKCS> phieuCans;
    private PhieuKCS phieuCan;

    @FXML
    public void inThongKe() {

    }

    @FXML
    public void actionEdit(ActionEvent event) {
//        MutableDateTime mutableDateTime = new MutableDateTime();
//        mutableDateTime.setDate(new Date().getTime());
//        mutableDateTime.addDays(-1);
//        if (phieuCan.thoiGian.getTime() < mutableDateTime.getMillis()) {
//            Dialog.showWarning("Quá thời gian sửa!");
//            return;
//        }
        int select = tbv_content.getSelectionModel().getFocusedIndex();
        if (phieuCan == null || phieuCan.getId() != phieuCans.get(select).getId()) {
            Dialog.showWarning("Vui lòng chọn bản ghi!");
            return;
        } else {
            State.setPhieuCan(phieuCan);
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/can/view/EditTicket.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Sửa phiếu");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.err.println(e.toString());
        }
    }

    @FXML
    public void onXemPhieu() {

        int select = tbv_content.getSelectionModel().getFocusedIndex();
        if (phieuCan == null || phieuCan.getId() != phieuCans.get(select).getId()) {
            Dialog.showWarning("Vui lòng chọn bản ghi!");
            return;
        } else {
            State.setPhieuCan(phieuCan);
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/can/view/ViewPhieuCan.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Xem phiếu cân");
                stage.setScene(new Scene(root));
                stage.setResizable(false);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void export_excel() {
        if (phieuCans == null || phieuCans.size() <= 0) {
            Dialog.showWarning("Không có bản ghi để xuất file");
        } else {

            try {
                FileChooser fileChooser = new FileChooser();

                String filename = "";
                filename += new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                filename = filename + "_" + State.getCaiDat().maChiNhanh + "_Thống kê KCS";

                fileChooser.setInitialFileName(filename);
                ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XLS files (*.xls)", "*.xls");
                fileChooser.getExtensionFilters().add(extFilter);
                File file = fileChooser.showSaveDialog(null);
                export(file);
                Dialog.showInformation("Đã xuất file thành công!");
            } catch (IOException ex) {
                Logger.getLogger(StatisticsController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi trong quá trình xuất file");
            } catch (WriteException ex) {
                Logger.getLogger(StatisticsController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi trong quá trình xuất file");
            }
        }
    }

    @FXML
    public void search() {
        String maPhieu = tf_maPhieu.getText();
        if (tf_maPhieu == null || "".equals(maPhieu)) {
            Dialog.showWarning("Vui lòng nhập mã phiếu!");
            return;
        }

        PhieuKCS phieuCan = null;
        try {
            List<PhieuKCS> temp = State.getEm().createNativeQuery("Select * from phieuKCS where soPhieu='" + maPhieu + "';", PhieuKCS.class).getResultList();
            if (temp == null || temp.size() == 0) {

            } else {
                phieuCan = temp.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Dialog.showError("Lỗi hệ thống");
            return;
        }
        if (null == phieuCan) {
            Dialog.showWarning("Phiếu cân không tồn tại!");
            return;
        }
        if (State.getUser().getRole() == 2) {
            //ok
        } else if (State.getUser().getRole() == 3 && State.getUser().getId() == phieuCan.user.getId()) {

        } else {
            Dialog.showWarning("Phiếu cân không tồn tại!");
            return;
        }
        if (phieuCans == null) {
            phieuCans = new ArrayList<>();
        } else {
            phieuCans.removeAll(phieuCans);
        }
        phieuCans.add(phieuCan);
        ObservableList<PhieuKCS> data = tbv_content.getItems();
        data.clear();
        for (PhieuKCS pc : phieuCans) {
            pc.updateProperty();
            data.add(pc);
        }
        tbv_content.setItems(data);

    }

    @FXML
    public void onThongKe() {
        try {
            String startDate = dp_startDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            LocalDate localDate = dp_endDate.getValue();
            String endDate = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            MutableDateTime date = new MutableDateTime();
            date.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(endDate).getTime());
            date.addDays(1);
            endDate = new SimpleDateFormat("yyyy-MM-dd").format(date.toDate());
            String sql = "SELECT * FROM PhieuKCS WHERE ";
            sql += " thoiGian >= '" + startDate + "' AND thoiGian <= '" + endDate + "'";
            if (can.util.State.getUser().getRole() == 2) {
                sql += " ;";
            } else if (can.util.State.getUser().getRole() == 3) {
                sql += " AND user_id= " + State.getUser().getId() + ";";
            } else {
                return;
            }
//            System.out.println(sql);
            phieuCans = State.getEm().createNativeQuery(sql, PhieuKCS.class).getResultList();

            ObservableList<PhieuKCS> data = tbv_content.getItems();
            data.clear();
            for (PhieuKCS pc : phieuCans) {
                pc.updateProperty();
                data.add(pc);
            }
            tbv_content.setItems(data);
        } catch (ParseException ex) {
            Logger.getLogger(StatisticsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        dp_startDate.setValue(LOCAL_DATE(new SimpleDateFormat("dd/MM/yyyy").format(new Date())));
        dp_endDate.setValue(LOCAL_DATE(new SimpleDateFormat("dd/MM/yyyy").format(new Date())));

        cl_soPhieu.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_soPhieu"));
        cl_tongTL.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tongTL"));
        cl_tlMun.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlMun"));
        cl_tlQuaCo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlQuaCo"));
        cl_tlVo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlVo"));
        cl_tlTapChat.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlTapChat"));
        cl_tyLeMun.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeMun"));
        cl_tyLeQuaCo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeQuaCo"));
        cl_tyLeVo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeVo"));
        cl_tyLeTapChat.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeTapChat"));
        cl_doKho.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_doKho"));
        cl_thoiGian.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_thoiGian"));


        tbv_content.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                phieuCan = newSelection;
            }
        });
    }

    public static final LocalDate LOCAL_DATE(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public void export(File file) throws IOException, WriteException {
        CaiDat chiNhanh = State.getCaiDat();

        WritableWorkbook workbook = Workbook.createWorkbook(file);
        // create sheet
        WritableSheet sheet1 = workbook.createSheet("sheet1", 0);

        sheet1.mergeCells(0, 0, 9, 0);
        sheet1.addCell(new Label(0, 0, chiNhanh.tenCty.toUpperCase(), CellStyleUtil.createFormatTitle(13, false, false)));
        sheet1.mergeCells(0, 1, 9, 1);
        sheet1.addCell(new Label(0, 1, "ĐC: " + chiNhanh.diaChiCty.toUpperCase(), CellStyleUtil.createFormatTitle(13, false, false)));
        sheet1.mergeCells(0, 2, 9, 2);
        sheet1.addCell(new Label(0, 2, chiNhanh.tenChiNhanh, CellStyleUtil.createFormatTitle(14, false, false)));

        sheet1.mergeCells(0, 3, 9, 3);
        sheet1.addCell(new Label(0, 3, "BẢNG THỐNG KÊ KIỂM ĐỊNH CHẤT LƯỢNG", CellStyleUtil.createFormatTitle(15, true, true)));
        sheet1.addCell(new Label(2, 4, "Từ ngày: " + dp_startDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), CellStyleUtil.createFormatTitle(13, false, false)));
        sheet1.addCell(new Label(2, 5, "Đến ngày: " + dp_endDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), CellStyleUtil.createFormatTitle(13, false, false)));

        sheet1.addCell(new Label(0, 6, "Số phiếu", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(1, 6, "Tổng TL", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(2, 6, "TL Mùn", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(3, 6, "TL Quá cỡ", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(4, 6, "TL Vỏ", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(5, 6, "TL Tạp chất", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(6, 6, "Tỷ lệ Mùn", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(7, 6, "Tỷ lê Quá cỡ", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(8, 6, "Tỷ lệ vỏ", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(9, 6, "Tỷ lệ Tạp chất", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(10, 6, "Độ khô", CellStyleUtil.createFormatTitleTable()));
        sheet1.addCell(new Label(11, 6, "Thời gian", CellStyleUtil.createFormatTitleTable()));

        for (int i = 0; i < phieuCans.size(); i++) {
            PhieuKCS pc = phieuCans.get(i);
            sheet1.addCell(new Label(0, 6 + 1 + i, pc.soPhieu + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(1, 6 + 1 + i, pc.tongTL + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(2, 6 + 1 + i, pc.tlMun + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(3, 6 + 1 + i, pc.tlQuaCo + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(4, 6 + 1 + i, pc.tlVo + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(5, 6 + 1 + i, pc.tlTapChat + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(6, 6 + 1 + i, pc.tyLeMun + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(7, 6 + 1 + i, pc.tyLeQuaCo + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(8, 6 + 1 + i, pc.tyLeVo + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(9, 6 + 1 + i, pc.tyLeTapChat + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(10, 6 + 1 + i, pc.doKho + "  ", CellStyleUtil.createFormatElementTable()));
            sheet1.addCell(new Label(11, 6 + 1 + i, new SimpleDateFormat("dd-MM-yyyy").format(pc.thoiGian), CellStyleUtil.createFormatElementTable()));

        }

        CellView cell = sheet1.getColumnView(0);
        cell.setAutosize(true);
//        sheet1.setColumnView(0, cell);
        sheet1.setColumnView(1, cell);
        sheet1.setColumnView(2, cell);
        sheet1.setColumnView(3, cell);
        sheet1.setColumnView(4, cell);
        sheet1.setColumnView(5, cell);
        sheet1.setColumnView(6, cell);
        sheet1.setColumnView(7, cell);
        sheet1.setColumnView(8, cell);
        sheet1.setColumnView(9, cell);
        sheet1.setColumnView(10, cell);
        sheet1.setColumnView(11, cell);

//        sheet1.addCell(new Label(2, cellTong + 3, "Khách hàng", CellStyleUtil.createFormatTitle(13, false, false)));
//        sheet1.addCell(new Label(5, cellTong + 3, "Giám sát", CellStyleUtil.createFormatTitle(13, false, false)));
//        sheet1.addCell(new Label(7, cellTong + 3, "Người cân", CellStyleUtil.createFormatTitle(13, false, false)));
        workbook.write();
        workbook.close();

    }

}
