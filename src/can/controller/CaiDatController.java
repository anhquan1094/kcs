/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.api.CaiDatAPI;
import can.dao.CaiDatJpaController;
import can.model.CaiDat;
import can.util.Dialog;
import can.util.State;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.EntityManager;

/**
 * FXML Controller class
 *
 * @author maxcd
 */
public class CaiDatController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public TextField tf_tenCty;
    @FXML
    public TextField tf_diaChi;
    @FXML
    public TextField tf_sdt;
    @FXML
    public TextField tf_fax;
    @FXML
    public TextField tf_maChiNhanh;
    @FXML
    public TextField tf_host;
    @FXML
    public Label lb_maUngDung;
    @FXML
    public Label lb_chiNhanh;
    
    private CaiDat chiNhanh;
    
    @FXML
    public void onDoiMa() {
        lb_maUngDung.setText(new Date().getTime() + "");
    }
    
    private void truncate() {
        try {
            Connection connection
                    = DriverManager.getConnection("jdbc:mysql://localhost/kcs", "root", "1234");
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("delete from phieukcs;");
            stmt.close();
            connection.close();
            EntityManager em = State.getEm();
        } catch (SQLException ex) {
            ex.printStackTrace();
            Dialog.showError("Không thể kết nối cơ sở dữ liệu!");
        }
    }
    
    @FXML
    public void onCapNhat() {
        String totalPCThuong = State.getEm().createNativeQuery("select count(id) as count from phieukcs where sync=0;").getSingleResult().toString();
        int total = Integer.parseInt(totalPCThuong);
        boolean confirm = Dialog.showConfirm("Toàn bộ dữ liệu sẽ bị mất sau khi cập nhật thành công, bạn còn " + total + " phiếu kcs chưa đồng bộ. Bạn có muốn tiếp tục không?");
        if (confirm == false) {
            return;
        }
        String maChiNhanh = tf_maChiNhanh.getText();
        String maUngDung = lb_maUngDung.getText();
        
        String value = CaiDatAPI.update(maUngDung, maChiNhanh);
        if ("success".equals(value)) {
            truncate();
            Dialog.showInformation("Cập nhật thành công");
        } else {
            Dialog.showError(value);
            return;
        }
        lb_chiNhanh.setText(State.getCaiDat().tenChiNhanh);
        tf_tenCty.setText(State.getCaiDat().tenCty);
        
    }
    
    @FXML
    public void onSave() {
        if (validate()) {
            try {
                chiNhanh = State.getCaiDat();
                chiNhanh.tenCty = tf_tenCty.getText();
                chiNhanh.diaChiCty = tf_diaChi.getText();
                chiNhanh.dtCty = tf_sdt.getText();
                chiNhanh.faxCty = tf_fax.getText();
                
                chiNhanh.host = tf_host.getText();
                
                new CaiDatJpaController(State.getEmf()).edit(chiNhanh);
                State.setCaiDat(chiNhanh);
            } catch (Exception ex) {
                Logger.getLogger(CaiDatController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi không thể lưu!");
                return;
            }
            Dialog.showInformation("Lưu thành công");
            onCancel();
            
        }
    }
    
    @FXML
    public void onCancel() {
        Stage stage = (Stage) lb_chiNhanh.getScene().getWindow();
        stage.close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chiNhanh = State.getCaiDat();
        
        tf_tenCty.setText(chiNhanh.tenCty);
        tf_diaChi.setText(chiNhanh.diaChiCty);
        tf_sdt.setText(chiNhanh.dtCty);
        tf_fax.setText(chiNhanh.faxCty);
        tf_maChiNhanh.setText(chiNhanh.maChiNhanh);
        tf_host.setText(chiNhanh.host + "");
        
        lb_maUngDung.setText(chiNhanh.keyApp);
        lb_chiNhanh.setText(chiNhanh.tenChiNhanh);
    }
    
    private boolean validate() {
        
        return true;
    }
    
}
