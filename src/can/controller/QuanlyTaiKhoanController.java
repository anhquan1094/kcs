/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.dao.UserJpaController;
import can.model.User;
import can.util.Dialog;
import can.util.State;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author maxcd
 */
public class QuanlyTaiKhoanController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public TableView<User> tbv_content;
    @FXML
    public TableColumn<User, String> cl_id;
    @FXML
    public TableColumn<User, String> cl_username;
    @FXML
    public TableColumn<User, String> cl_role;
    @FXML
    public TableColumn<User, String> cl_chucDanh;

    private List<User> users;
    private User user;

    @FXML
    public void onEdit() {
        if (State.getUser().getRole() == 2) {
            Dialog.showWarning("Bạn không được quyền thực hiện chức năng này!");
            return;
        }
        int select = tbv_content.getSelectionModel().getFocusedIndex();
        if (user == null || user.getId() != users.get(select).getId()) {
            Dialog.showWarning("Vui lòng chọn bản ghi!");
            return;
        } else {
            try {
                if (user.getRole() == 2) {
                    user.setRole(3);
                    Dialog.showInformation("Bạn vừa thu quyền quản lý thành công");
                } else if (user.getRole() == 3) {
                    user.setRole(2);
                    Dialog.showInformation("Bạn vừa cấp quyền quản lý thành công");
                }
                new UserJpaController(State.getEmf()).edit(user);
            } catch (Exception ex) {
                Logger.getLogger(QuanlyTaiKhoanController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi không thể lưu");
            }
            user = null;
            reload();
        }
    }

    @FXML
    public void onDelete() {

        int select = tbv_content.getSelectionModel().getFocusedIndex();
        if (user == null || user.getId() != users.get(select).getId()) {
            Dialog.showWarning("Vui lòng chọn bản ghi!");
            return;
        } else {
            try {
                user.available = false;
                new UserJpaController(State.getEmf()).edit(user);
            } catch (Exception ex) {
                Logger.getLogger(QuanlyTaiKhoanController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi không thể xóa");
            }
            Dialog.showInformation("Xóa thành công");
            user = null;
            reload();
        }
    }

    @FXML
    public void reload() {
        if (State.getUser().getRole() == 2) {
            users = State.getEm().createNativeQuery("Select * from user where role>2 and available=1;", User.class).getResultList();
        } else {
            users = State.getEm().createNativeQuery("Select * from user where role>1 and available=1;", User.class).getResultList();
        }
        ObservableList<User> data = tbv_content.getItems();
        data.clear();
        for (User pc : users) {
            pc.updateProperty();
            data.add(pc);
        }
        tbv_content.setItems(data);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        cl_id.setCellValueFactory(new PropertyValueFactory<User, String>("cl_id"));
        cl_username.setCellValueFactory(new PropertyValueFactory<User, String>("cl_username"));
        cl_role.setCellValueFactory(new PropertyValueFactory<User, String>("cl_role"));
        tbv_content.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                user = newSelection;
            }
        });

        reload();
    }

}
