/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.api.PushPhieuKCS;
import can.dao.PhieuKCSJpaController;
import can.model.PhieuKCS;
import can.util.CheckInternet;
import can.util.Dialog;
import can.util.FormatNumber;
import can.util.State;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author VMT
 */
public class EditTicketController implements Initializable {

    @FXML
    public Label lb_soPhieu;
    @FXML
    public TextField tf_tlTong;
    @FXML
    public TextField tf_tlMun;
    @FXML
    public TextField tf_tlQuaCo;
    @FXML
    public TextField tf_tlVo;
    @FXML
    public TextField tf_tlTapChat;
    @FXML
    public TextField tf_doKho;
    private PhieuKCS phieuKCS;

    @FXML
    public void actionClose(ActionEvent event) {
        Stage stage = (Stage) tf_doKho.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void actionUpdate(ActionEvent event) {
        if (validateAll()) {
            tinhTyLe();
            phieuKCS.sync = false;
            try {
                new PhieuKCSJpaController(State.getEmf()).edit(phieuKCS);
                Dialog.showInformation("Cập thành công");
                if (CheckInternet.netIsAvailable()) {
                    new PushPhieuKCS().run();
                }
            } catch (Exception ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi không thể lưu");
            }
        }
        actionClose(event);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        phieuKCS = State.getPhieuCan();

        lb_soPhieu.setText("Số phiếu: " + phieuKCS.soPhieu);
        tf_tlTong.setText(phieuKCS.tongTL + "");
        tf_tlMun.setText(phieuKCS.tlMun + "");
        tf_tlQuaCo.setText(phieuKCS.tlQuaCo + "");
        tf_tlVo.setText(phieuKCS.tlVo + "");
        tf_tlTapChat.setText(phieuKCS.tlTapChat + "");
        tf_doKho.setText(phieuKCS.doKho + "");
        ArrayList<String> list_loaiHang = new ArrayList<>();
        ArrayList<String> list_kieuSP = new ArrayList<>();

    }

    public void tinhTyLe() {
        if (validateTyLe()) {
            phieuKCS.tyLeMun = FormatNumber.rounding((phieuKCS.tlMun / phieuKCS.tongTL) * 100);
            phieuKCS.tyLeQuaCo = FormatNumber.rounding((phieuKCS.tlQuaCo / phieuKCS.tongTL) * 100);
            phieuKCS.tyLeVo = FormatNumber.rounding((phieuKCS.tlVo / phieuKCS.tongTL) * 100);
            phieuKCS.tyLeTapChat = FormatNumber.rounding((phieuKCS.tlTapChat / phieuKCS.tongTL) * 100);
        }
    }

    private boolean validateAll() {
        if (!validateTyLe()) {
            return false;
        }
        if (!tf_doKho.getText().equals("")) {
            try {
                phieuKCS.doKho = Float.parseFloat(tf_doKho.getText());
            } catch (Exception e) {
                Dialog.showWarning("Độ khô sai định dạng");
                return false;
            }
        }

        return true;
    }

    private boolean validateTyLe() {
        try {
            phieuKCS.tongTL = Float.parseFloat(tf_tlTong.getText());
            phieuKCS.tlMun = Float.parseFloat(tf_tlMun.getText());
            phieuKCS.tlQuaCo = Float.parseFloat(tf_tlQuaCo.getText());
            phieuKCS.tlVo = Float.parseFloat(tf_tlVo.getText());
            phieuKCS.tlTapChat = Float.parseFloat(tf_tlTapChat.getText());

        } catch (Exception e) {
            Dialog.showWarning("Bạn nhập thiếu thông tin hoặc sai định dạng số");
            return false;
        }
        if (phieuKCS.tongTL <= 0) {
            Dialog.showWarning("Trong lượng tổng phải lớn hơn 0");
            return false;
        }
        if (phieuKCS.tongTL < phieuKCS.tlMun
                || phieuKCS.tongTL < phieuKCS.tlQuaCo
                || phieuKCS.tongTL < phieuKCS.tlVo
                || phieuKCS.tongTL < phieuKCS.tlTapChat) {
            Dialog.showWarning("Trong lượng tổng phải lớn nhất!");
            return false;
        }
        return true;
    }

}
