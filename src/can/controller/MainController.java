/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.Can;
import can.api.PushPhieuKCS;
import can.dao.PhieuKCSJpaController;
import can.model.PhieuKCS;
import can.util.CheckInternet;
import can.util.Dialog;
import can.util.FormatNumber;
import can.util.PhieuCanPrinter;
import can.util.State;
import com.jfoenix.controls.JFXButton;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.SwingUtilities;

/**
 *
 * @author VMT
 */
public class MainController implements Initializable {

    @FXML
    public ImageView iv_logout;

    @FXML
    public Label lb_date_time;

    @FXML
    public Label lb_tlMun;
    @FXML
    public Label lb_tlQuaCo;
    @FXML
    public Label lb_tlVo;
    @FXML
    public Label lb_tlTapChat;

    @FXML
    public TextField tf_soPhieu;
    @FXML
    public TextField tf_tlTong;
    @FXML
    public TextField tf_tlMun;
    @FXML
    public TextField tf_tlQuaCo;
    @FXML
    public TextField tf_tlVo;
    @FXML
    public TextField tf_tlTapChat;
    @FXML
    public TextField tf_doKho;

    @FXML
    public TableView<PhieuKCS> tbv_content;
    @FXML
    public TableColumn<PhieuKCS, String> cl_soPhieu;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tongTL;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlMun;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlQuaCo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlVo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tlTapChat;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeMun;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeQuaCo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeVo;
    @FXML
    public TableColumn<PhieuKCS, String> cl_tyLeTapChat;
    @FXML
    public TableColumn<PhieuKCS, String> cl_doKho;
    @FXML
    public TableColumn<PhieuKCS, String> cl_thoiGian;

    private List<PhieuKCS> phieuCans;
    private ObservableList<PhieuKCS> data;
    private PhieuKCS phieuKCS;

    @FXML
    public void logout() {
        try {

            Parent root = FXMLLoader.load(getClass().getResource("/can/view/Login.fxml"));
            Can.primaryStage.setScene(new Scene(root));
            Can.primaryStage.setTitle("Đăng nhập");
            Can.primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void save() {
        if (validateAll()) {
            tinhTyLe();
            phieuKCS.thoiGian = new Date();
            phieuKCS.user = State.getUser();
            phieuKCS.sync = false;
            try {
                new PhieuKCSJpaController(State.getEmf()).create(phieuKCS);
                addRowTable(phieuKCS);
                Dialog.showInformation("Lưu thành công");
                initData();
                if (CheckInternet.netIsAvailable()) {
                    new PushPhieuKCS().run();
                }
            } catch (Exception ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                Dialog.showError("Lỗi không thể lưu");
            }
        }
    }

    @FXML
    public void tinhTyLe() {
        if (validateTyLe()) {
            phieuKCS.tyLeMun = FormatNumber.rounding((phieuKCS.tlMun / phieuKCS.tongTL) * 100);
            phieuKCS.tyLeQuaCo = FormatNumber.rounding((phieuKCS.tlQuaCo / phieuKCS.tongTL) * 100);
            phieuKCS.tyLeVo = FormatNumber.rounding((phieuKCS.tlVo / phieuKCS.tongTL) * 100);
            phieuKCS.tyLeTapChat = FormatNumber.rounding((phieuKCS.tlTapChat / phieuKCS.tongTL) * 100);

            lb_tlMun.setText(phieuKCS.tyLeMun + "");
            lb_tlQuaCo.setText(phieuKCS.tyLeQuaCo + "");
            lb_tlVo.setText(phieuKCS.tyLeVo + "");
            lb_tlTapChat.setText(phieuKCS.tyLeTapChat + "");
        }
    }

    @FXML
    public void phieuMoi(ActionEvent event) {
        initData();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        initData();
        initTable();
        ArrayList<String> list_loaiHang = new ArrayList<>();
        ArrayList<String> list_kieuSP = new ArrayList<>();

        setTime();
    }

    private void initData() {
        lb_tlMun.setText("");
        lb_tlQuaCo.setText("");
        lb_tlVo.setText("");
        lb_tlTapChat.setText("");

        tf_soPhieu.setText("");
        tf_tlTong.setText("");
        tf_tlMun.setText("0");
        tf_tlQuaCo.setText("0");
        tf_tlVo.setText("0");
        tf_tlTapChat.setText("0");
        tf_doKho.setText("");
        phieuKCS = new PhieuKCS();
    }

    private void initTable() {
        try {

            cl_soPhieu.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_soPhieu"));
            cl_tongTL.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tongTL"));
            cl_tlMun.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlMun"));
            cl_tlQuaCo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlQuaCo"));
            cl_tlVo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlVo"));
            cl_tlTapChat.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tlTapChat"));
            cl_tyLeMun.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeMun"));
            cl_tyLeQuaCo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeQuaCo"));
            cl_tyLeVo.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeVo"));
            cl_tyLeTapChat.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_tyLeTapChat"));
            cl_doKho.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_doKho"));
            cl_thoiGian.setCellValueFactory(new PropertyValueFactory<PhieuKCS, String>("cl_thoiGian"));

//TODO
            String sql = "SELECT * FROM phieukcs where phieukcs.thoiGian > DATE_SUB(current_timestamp(),INTERVAL 1 day) ";
            if (can.util.State.getUser().getRole() == 2) {
                sql += " ;";
            } else if (can.util.State.getUser().getRole() == 3) {
                sql += " AND user_id= " + State.getUser().getId() + ";";
            } else {
                return;
            }

            EntityManager em = State.getEm();
            Query query = em.createNativeQuery(sql, PhieuKCS.class);
            phieuCans = query.getResultList();

            data = tbv_content.getItems();
            for (PhieuKCS pc : phieuCans) {
                pc.updateProperty();
                data.add(pc);
            }
            tbv_content.setItems(data);

            MenuItem mi1 = new MenuItem("Xem phiếu");
            ContextMenu menu = new ContextMenu();

            mi1.setOnAction((ActionEvent event) -> {
                PhieuKCS item = tbv_content.getSelectionModel().getSelectedItem();
                menu.hide();
                State.setPhieuCan(item);
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/can/view/ViewPhieuCan.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Xem phiếu cân");
                    stage.setScene(new Scene(root));
                    stage.setResizable(false);
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            MenuItem mi2 = new MenuItem("Sửa phiếu");
            mi2.setOnAction((ActionEvent event) -> {
                PhieuKCS item = tbv_content.getSelectionModel().getSelectedItem();
                menu.hide();
                State.setPhieuCan(item);
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/can/view/EditTicket.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Sửa phiếu");
                    stage.setScene(new Scene(root));
                    stage.setResizable(false);
                    stage.show();
                } catch (IOException e) {
                    System.err.println(e.toString());
                }
            });

            menu.getItems().add(mi1);
            menu.getItems().add(mi2);
            tbv_content.setContextMenu(menu);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addRowTable(PhieuKCS pc) {
        phieuCans.add(pc);
        pc.updateProperty();
        data.add(pc);
    }

    private boolean validateAll() {
        phieuKCS.soPhieu = tf_soPhieu.getText();
        if (phieuKCS.soPhieu.equals("")) {
            Dialog.showWarning("Bạn chưa nhập số phiếu");
            return false;
        } else {
            List<PhieuKCS> temp = State.getEm().createNativeQuery("Select * from phieuKCS where soPhieu='" + phieuKCS.soPhieu + "';", PhieuKCS.class).getResultList();
            if (temp == null || temp.size() == 0) {

            } else {
                Dialog.showWarning("Số phiếu đã tồn tại");
                return false;
            }

        }
        if (!validateTyLe()) {
            return false;
        }
        if (!tf_doKho.getText().equals("")) {
            try {
                phieuKCS.doKho = Float.parseFloat(tf_doKho.getText());
            } catch (Exception e) {
                Dialog.showWarning("Độ khô sai định dạng");
                return false;
            }
        }

        return true;
    }

    private boolean validateTyLe() {
        try {
            phieuKCS.tongTL = Float.parseFloat(tf_tlTong.getText());
            phieuKCS.tlMun = Float.parseFloat(tf_tlMun.getText());
            phieuKCS.tlQuaCo = Float.parseFloat(tf_tlQuaCo.getText());
            phieuKCS.tlVo = Float.parseFloat(tf_tlVo.getText());
            phieuKCS.tlTapChat = Float.parseFloat(tf_tlTapChat.getText());

        } catch (Exception e) {
            Dialog.showWarning("Bạn nhập thiếu thông tin hoặc sai định dạng số");
            return false;
        }
        if (phieuKCS.tongTL <= 0) {
            Dialog.showWarning("Trong lượng tổng phải lớn hơn 0");
            return false;
        }
        if (phieuKCS.tongTL < phieuKCS.tlMun
                || phieuKCS.tongTL < phieuKCS.tlQuaCo
                || phieuKCS.tongTL < phieuKCS.tlVo
                || phieuKCS.tongTL < phieuKCS.tlTapChat) {
            Dialog.showWarning("Trong lượng tổng phải lớn nhất!");
            return false;
        }
        return true;
    }

    public void setTime() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date();
                    lb_date_time.setText(dateFormat.format(date));
                });
            }
        }, 500, 500);
    }

}
