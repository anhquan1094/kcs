/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.Can;
import can.api.CaiDatAPI;
import can.api.LoginAPI;
import can.api.PushPhieuKCS;
import can.dao.UserJpaController;
import can.model.User;
import can.util.CheckInternet;
import can.util.Dialog;
import can.util.RequestHttp;
import can.util.State;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author VMT
 */
public class LoginController implements Initializable {

    @FXML
    public TextField tf_username;
    @FXML
    public PasswordField tf_password;

    public void handleButtonAction() {
        String username = tf_username.getText();
        String password = tf_password.getText();
        if (username.equals("root")) {

            User user = new UserJpaController(State.getEmf()).findUser(0l);
            if (user == null) {
                try {
                    user = new User();
                    user.setId(0l);
                    user.setUsername("root");
                    user.setPassword("12345");
                    user.setRole(1);
                    user.available = true;
                    new UserJpaController(State.getEmf()).create(user);
                } catch (Exception ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                    Dialog.showError("Lỗi không thể khởi tạo tài khoản root!");
                    return;
                }
            }
            if (password.equals(user.getPassword())) {
                State.setUser(user);
            } else {
                Dialog.showWarning("Bạn nhập sai tài khoản hoặc mật khẩu");
                return;
            }

        } else if (CheckInternet.netIsAvailable()) {
            if (!LoginAPI.checkLogin(username, password)) {
                Dialog.showWarning("Bạn nhập sai tài khoản hoặc mật khẩu");
                return;
            } else {
                new PushPhieuKCS().run();
            }
        } else {
            String sql = "Select * from User where username=? and password=? and available=1;";
            Query query = State.getEm().createNativeQuery(sql, User.class);
            query.setParameter(1, username);
            query.setParameter(2, password);
            User user;
            try {
                user = (User) query.getSingleResult();
                if (user == null) {
                    Dialog.showWarning("Bạn nhập sai tài khoản hoặc mật khẩu!");
                    return;
                }
            } catch (Exception e) {
                // e.printStackTrace();
                Dialog.showWarning("Bạn nhập sai tài khoản hoặc mật khẩu!");
                return;
            }
            State.setUser(user);
        }
        // neu co Jsup api
        // update user on DB

        //ko co thi check DB
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/can/view/Main.fxml"));
            Can.primaryStage.setScene(new Scene(root));
            Can.primaryStage.setTitle("Kiểm định chất lượng");
            Can.primaryStage.setResizable(false);
            Can.primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
            Dialog.showError(e.getMessage());
        }
    }

    @FXML
    public void onEnter() {
//        Stage stage = (Stage) tf_username.getScene().getWindow();
//        stage.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
//            if (ev.getCode() == KeyCode.ENTER) {
//                handleButtonAction();
//                //ev.consume();
//            }
//        });
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

}
