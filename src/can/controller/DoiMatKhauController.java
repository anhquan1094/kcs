/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.controller;

import can.api.DoiMatKhauAPI;
import can.dao.UserJpaController;
import can.model.User;
import can.util.Dialog;
import can.util.State;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author maxcd
 */
public class DoiMatKhauController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private PasswordField tf_mkCu;
    @FXML
    private PasswordField tf_mkMoi1;
    @FXML
    private PasswordField tf_mkMoi2;

    @FXML
    public void save() {
        if (!tf_mkCu.getText().equals(State.getUser().getPassword())) {
            Dialog.showWarning("Mật khẩu cũ không đúng");
            return;
        }
        if (!tf_mkMoi1.getText().equals(tf_mkMoi2.getText())) {
            Dialog.showWarning("Mật khẩu mới không khớp");
            return;
        }
        if (tf_mkMoi1.getText().equals("")) {
            Dialog.showWarning("Bạn chưa nhập mật khẩu mới");
            return;
        }
        boolean flag = false;
        //root
        if (State.getUser().getId() == 0l) {
            try {
                User user = State.getUser();
                user.setPassword(tf_mkMoi1.getText());
                new UserJpaController(State.getEmf()).edit(user);
                State.setUser(user);
                flag = true;
            } catch (Exception ex) {
                Logger.getLogger(DoiMatKhauController.class.getName()).log(Level.SEVERE, null, ex);
                flag = false;
            }
        } else {
            flag = DoiMatKhauAPI.excute(tf_mkCu.getText(), tf_mkMoi1.getText());
        }

        if (flag) {
            Dialog.showInformation("Đổi mật khẩu thành công");
            cancel();
        } else {
            Dialog.showError("Lỗi không thể đổi mật khẩu");
        }

    }

    @FXML
    public void cancel() {
        Stage stage = (Stage) tf_mkMoi2.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
