/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.dao;

import can.dao.exceptions.NonexistentEntityException;
import can.dao.exceptions.PreexistingEntityException;
import can.model.PhieuKCS;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author maxcd
 */
public class PhieuKCSJpaController implements Serializable {

    public PhieuKCSJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PhieuKCS phieuKCS) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(phieuKCS);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPhieuKCS(phieuKCS.getId()) != null) {
                throw new PreexistingEntityException("PhieuKCS " + phieuKCS + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PhieuKCS phieuKCS) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            phieuKCS = em.merge(phieuKCS);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = phieuKCS.getId();
                if (findPhieuKCS(id) == null) {
                    throw new NonexistentEntityException("The phieuKCS with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PhieuKCS phieuKCS;
            try {
                phieuKCS = em.getReference(PhieuKCS.class, id);
                phieuKCS.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The phieuKCS with id " + id + " no longer exists.", enfe);
            }
            em.remove(phieuKCS);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PhieuKCS> findPhieuKCSEntities() {
        return findPhieuKCSEntities(true, -1, -1);
    }

    public List<PhieuKCS> findPhieuKCSEntities(int maxResults, int firstResult) {
        return findPhieuKCSEntities(false, maxResults, firstResult);
    }

    private List<PhieuKCS> findPhieuKCSEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PhieuKCS.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PhieuKCS findPhieuKCS(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PhieuKCS.class, id);
        } finally {
            em.close();
        }
    }

    public int getPhieuKCSCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PhieuKCS> rt = cq.from(PhieuKCS.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
