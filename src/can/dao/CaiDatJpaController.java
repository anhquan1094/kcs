/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.dao;

import can.dao.exceptions.NonexistentEntityException;
import can.dao.exceptions.PreexistingEntityException;
import can.model.CaiDat;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author maxcd
 */
public class CaiDatJpaController implements Serializable {

    public CaiDatJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CaiDat caiDat) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(caiDat);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCaiDat(caiDat.getId()) != null) {
                throw new PreexistingEntityException("CaiDat " + caiDat + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CaiDat caiDat) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            caiDat = em.merge(caiDat);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = caiDat.getId();
                if (findCaiDat(id) == null) {
                    throw new NonexistentEntityException("The caiDat with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CaiDat caiDat;
            try {
                caiDat = em.getReference(CaiDat.class, id);
                caiDat.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The caiDat with id " + id + " no longer exists.", enfe);
            }
            em.remove(caiDat);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CaiDat> findCaiDatEntities() {
        return findCaiDatEntities(true, -1, -1);
    }

    public List<CaiDat> findCaiDatEntities(int maxResults, int firstResult) {
        return findCaiDatEntities(false, maxResults, firstResult);
    }

    private List<CaiDat> findCaiDatEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CaiDat.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CaiDat findCaiDat(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CaiDat.class, id);
        } finally {
            em.close();
        }
    }

    public int getCaiDatCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CaiDat> rt = cq.from(CaiDat.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
