/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.util;

import can.model.CaiDat;
import can.model.PhieuKCS;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.text.AttributedCharacterIterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author maxcd
 */
public class PhieuCanPrinter implements Printable {

    private PhieuKCS pc;

    public PhieuCanPrinter(PhieuKCS pc) {
        this.pc = pc;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

        /* User (0,0) is typically outside the imageable area, so we must
         * translate by the X and Y values in the PageFormat to avoid clipping
         */
        if (pageIndex > 0) {
            /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }
        //Paper paper = pageFormat.getPaper();
        // paper.setImageableArea(0, 0, 842, 595);//Size(842,595);
        // paper.setSize(595, 420 );
        // pageFormat.setPaper(paper);
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        /* Now we perform our rendering */
        int dandong = 18;

        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 14));
        int xTop = 45, ytop = 40;

        CaiDat chiNhanh = State.getCaiDat();
        g2d.drawString(chiNhanh.tenCty.toUpperCase(), xTop, ytop);
        g2d.drawString("Chi nhánh: " + chiNhanh.tenChiNhanh, xTop, ytop + dandong);
        g2d.drawString("SDT: " + chiNhanh.dtCty, xTop, ytop + 2 * dandong);
        g2d.drawString("FAX: " + chiNhanh.faxCty, xTop, ytop + 3 * dandong);

        g2d.setFont(new Font("TimesRoman", Font.BOLD, 15));
        g2d.drawString("Phiếu kiểm tra chất lượng", 230, 90 + dandong);
        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 14));
        g2d.drawString("Số phiếu: " + pc.soPhieu, 380, 95 + 2 * dandong);

        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 14));

        int xBody1 = 45, yBody = 155, xBody2 = 180, xBody3 = 190, xBody4 = 335, xBody5 = 430, xBody6 = 440;
        g2d.drawString("Tổng trọng lượng", xBody1, yBody);
        g2d.drawString("Trọng lượng mùn", xBody1, yBody + 1 * dandong);
        g2d.drawString("Trọng lượng quá cỡ", xBody1, yBody + 2 * dandong);
        g2d.drawString("Trọng lượng vỏ", xBody1, yBody + 3 * dandong);
        g2d.drawString("Trọng lượng tạp chất", xBody1, yBody + 4 * dandong);
        g2d.drawString("Đọ ẩm", xBody1, yBody + 5 * dandong);
//        g2d.drawString("Loại hàng", xBody1, yBody + 6 * dandong);
//        g2d.drawString("Kiểu sản phẩm", xBody1, yBody + 7 * dandong);

        g2d.drawString(":", xBody2, yBody);
        g2d.drawString(":", xBody2, yBody + 1 * dandong);
        g2d.drawString(":", xBody2, yBody + 2 * dandong);
        g2d.drawString(":", xBody2, yBody + 3 * dandong);
        g2d.drawString(":", xBody2, yBody + 4 * dandong);
        g2d.drawString(":", xBody2, yBody + 5 * dandong);
//        g2d.drawString(":", xBody2, yBody + 6 * dandong);
//        g2d.drawString(":", xBody2, yBody + 7 * dandong);

        g2d.drawString(convertString(pc.tongTL + "", 10) + " gram", xBody3, yBody);
        g2d.drawString(convertString(pc.tlMun + "", 10) + " gram", xBody3, yBody + 1 * dandong);
        g2d.drawString(convertString(pc.tlQuaCo + "", 10) + " gram", xBody3, yBody + 2 * dandong);
        g2d.drawString(convertString(pc.tlVo + "", 10) + " gram", xBody3, yBody + 3 * dandong);
        g2d.drawString(convertString(pc.tlTapChat + "", 10) + " gram", xBody3, yBody + 4 * dandong);
        g2d.drawString(convertString(pc.doKho + "", 10) + " %", xBody3, yBody + 5 * dandong);
//        g2d.drawString(pc.loaiHang.tenLoai + "" , xBody3, yBody + 6* dandong);
//        g2d.drawString(KieuSanPham.toString(pc.kieuSP), xBody3, yBody + 7 * dandong);

        g2d.drawString("Tỷ lệ mùn", xBody4, yBody + dandong);
        g2d.drawString("Tỷ lệ quá cỡ", xBody4, yBody + 2 * dandong);
        g2d.drawString("Tỷ lệ vỏ", xBody4, yBody + 3 * dandong);
        g2d.drawString("Tỷ lệ tạp chất", xBody4, yBody + 4 * dandong);

        g2d.drawString(":", xBody5, yBody + 1 * dandong);
        g2d.drawString(":", xBody5, yBody + 2 * dandong);
        g2d.drawString(":", xBody5, yBody + 3 * dandong);
        g2d.drawString(":", xBody5, yBody + 4 * dandong);

        g2d.drawString(convertString(pc.tyLeMun + "", 10) + " %", xBody6, yBody + 1 * dandong);
        g2d.drawString(convertString(pc.tyLeQuaCo + "", 10) + " %", xBody6, yBody + 2 * dandong);
        g2d.drawString(convertString(pc.tyLeVo + "", 10) + " %", xBody6, yBody + 3 * dandong);
        g2d.drawString(convertString(pc.tyLeTapChat + "", 10) + " %", xBody6, yBody + 4 * dandong);
//        g2d.drawString("Khách hàng", 100, 270);
//        g2d.drawString("Tài xế", 280, 270);
//        g2d.drawString("Người cân", 440, 270);
        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    }

    private String convertString(String str, int length) {
        String value = "                             ";
        value += str;
        int size = value.length();
        value = value.substring(size - length);
        return value.replaceAll(" ", "  ");
    }
;
/*
    public void actionPerformed(ActionEvent e) {
         PrinterJob job = PrinterJob.getPrinterJob();
         job.setPrintable(this);
         boolean ok = job.printDialog();
         if (ok) {
             try {
                  job.print();
             } catch (PrinterException ex) {
               The job did not successfully complete 
             }
         }
    }
 
    public static void main(String args[]) {
  
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        JFrame f = new JFrame("Hello World Printer");
        f.addWindowListener(new WindowAdapter() {
           public void windowClosing(WindowEvent e) {System.exit(0);}
        });
        JButton printButton = new JButton("Print Hello World");
        printButton.addActionListener(new HelloWorldPrinter());
        f.add("Center", printButton);
        f.pack();
        f.setVisible(true);
    }
 */

}
