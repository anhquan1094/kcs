/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author maxcd
 */
public class CheckInternet {

    public static boolean netIsAvailable() {
        try {
            final URL url = new URL(State.getCaiDat().host);
            final URLConnection conn = url.openConnection();
            conn.setConnectTimeout(500);
            conn.connect();
            return true;
        } catch (MalformedURLException e) {
            System.out.println("No internet");
            return false;
        } catch (IOException e) {
            System.out.println("No internet");
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(netIsAvailable());
    }
}
