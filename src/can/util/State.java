/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.util;

import can.dao.CaiDatJpaController;
import can.model.CaiDat;
import can.model.PhieuKCS;
import can.model.User;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author maxcd
 */
public class State {

    private static EntityManagerFactory emf;
    private static EntityManager em;
    private static User user;
    private static CaiDat caiDat;
    private static PhieuKCS phieuCan;

    public static EntityManagerFactory getEmf() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("CanPU");
        }
        return emf;
    }

    public static void refreshEmf() {
        emf = Persistence.createEntityManagerFactory("CanPU");
    }

    public static EntityManager getEm() {
//        if (em == null) {
//            em = getEmf().createEntityManager();
//        }
//        return em;
        return getEmf().createEntityManager();
    }


    public static PhieuKCS getPhieuCan() {
        return phieuCan;
    }

    public static void setPhieuCan(PhieuKCS phieuCan) {
        State.phieuCan = phieuCan;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        State.user = user;
    }

    public static void setCaiDat(CaiDat caiDat) {
        State.caiDat = caiDat;
    }

    public static CaiDat getCaiDat() {
        if (caiDat == null) {
            caiDat = new CaiDatJpaController(getEmf()).findCaiDat(1l);
            if (caiDat == null) {
                System.out.println("Chi nhanh null");
                try {
                    caiDat = new CaiDat();
                    caiDat.keyApp = new Date().getTime() + "";
                    new CaiDatJpaController(getEmf()).create(caiDat);
                } catch (Exception ex) {
                    Logger.getLogger(State.class.getName()).log(Level.SEVERE, null, ex);
                    Dialog.showError("Không khởi tạo được thông tin công ty và chi nhánh");
                    return null;
                }
            }
        }
        return caiDat;
    }

}
