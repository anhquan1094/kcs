package can.util;

import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;

/**
 * Created by AnhQuan on 11/10/2016.
 */
public class CellStyleUtil {

    public static WritableCellFormat createFormatTitle(int sizeText, boolean bold, boolean center) throws WriteException {
//        Colour colour = (b == true) ? Colour.GREEN : Colour.RED;
        WritableFont wfontStatus;
        if (bold) {
            wfontStatus = new WritableFont(WritableFont.createFont("Times New Roman"), sizeText, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE);
        } else {
            wfontStatus = new WritableFont(WritableFont.createFont("Times New Roman"), sizeText, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE);
        }
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);

        fCellstatus.setWrap(false);
        if (center) {
            fCellstatus.setAlignment(jxl.format.Alignment.CENTRE);

        } else {
            fCellstatus.setAlignment(jxl.format.Alignment.LEFT);
        }
            fCellstatus.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
//        fCellstatus.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM, jxl.format.Colour.BLUE2);
        return fCellstatus;
    }

    public static WritableCellFormat createFormatTitleTable() throws WriteException {
        //Colour colour = Colour.LIGHT_GREEN;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Times New Roman"), 13, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);
        // fCellstatus.setBackground(colour);
        fCellstatus.setWrap(false);
        fCellstatus.setAlignment(jxl.format.Alignment.CENTRE);
        fCellstatus.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        fCellstatus.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
        return fCellstatus;
    }

    public static WritableCellFormat createFormatElementTable() throws WriteException {
        //  Colour colour = Colour.LIGHT_TURQUOISE;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Times New Roman"), 13, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus);
//        if(setColor){
//            fCellstatus.setBackground(colour);
//        }
        fCellstatus.setWrap(true);
        fCellstatus.setAlignment(Alignment.CENTRE);
        fCellstatus.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        fCellstatus.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
        return fCellstatus;
    }

    public static WritableCellFormat createFormatElementNumberTable(boolean setColor) throws WriteException {
        Colour colour = Colour.LIGHT_TURQUOISE;
        WritableFont wfontStatus = new WritableFont(WritableFont.createFont("Times New Roman"), 12, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE);
        WritableCellFormat fCellstatus = new WritableCellFormat(wfontStatus, new NumberFormat("#0.0"));
        if (setColor) {
            fCellstatus.setBackground(colour);
        }
        fCellstatus.setWrap(true);
        fCellstatus.setAlignment(Alignment.CENTRE);
        fCellstatus.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        fCellstatus.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN);
        return fCellstatus;
    }
}
