/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.util;

/**
 *
 * @author maxcd
 */
public class FormatNumber {

    public static float rounding(float n) {
        String a = n + "";
        String b[] = a.split("\\.");
        String v;
        if (b.length == 2) {
            v = b[0] + ".";
            String c = b[1];
            if (c.length() > 2) {
                v += c.substring(0, 3);
            } else {
                v += c;
            }
            return Float.parseFloat(v);
        } else {
            return n;
        }
    }
;
}
