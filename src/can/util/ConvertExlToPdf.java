/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.util;

import com.aspose.cells.FileFormatType;
import com.aspose.cells.PdfSaveOptions;
import com.aspose.cells.Workbook;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author maxcd
 */
public class ConvertExlToPdf {
    public static void convertToPdf(InputStream in, OutputStream out) throws Exception{
            Workbook workbook=new Workbook(in);
            PdfSaveOptions pdfSaveOptions = new PdfSaveOptions();
            pdfSaveOptions.setAllColumnsInOnePagePerSheet(true);
            workbook.save(out, pdfSaveOptions);
            in.close();
            out.close();
       
    }
}
