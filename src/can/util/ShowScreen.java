/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.util;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author maxcd
 */
public class ShowScreen {

    public ShowScreen() {
    }

    public void show(String screen, String title) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/can/view/" + screen));
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(new Scene(root));
        stage.show();
    }

    public Parent getParent(String screen) throws IOException {
        return FXMLLoader.load(getClass().getResource("/can/view/" + screen));
    }
}
