/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author maxcd
 */
@Entity
public class CaiDat implements Serializable {

    @Id
    private Long id = 1l;
    public String idChiNhanh = "";
    public String maChiNhanh = "";
    public String tenChiNhanh = "";
    public String diaChiChiNhanh = "";
    public String tenCty = "";
    public String idCty = "";
    public String diaChiCty = "";
    public String dtCty = "";
    public String faxCty = "";

    public String baudrate = "4800";
    public int portCom = 1;

    public String keyApp = "";
    public String host = "";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaiDat)) {
            return false;
        }
        CaiDat other = (CaiDat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "can.model.ChiNhanh[ id=" + id + " ]";
    }

}
