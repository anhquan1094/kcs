/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.model;

import can.util.State;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author maxcd
 */
@Entity
public class PhieuKCS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    public String soPhieu;
    public float tongTL;
    public float tlMun;
    public float tlQuaCo;
    public float tlVo;
    public float tlTapChat;
    public float tyLeMun;
    public float tyLeQuaCo;
    public float tyLeVo;
    public float tyLeTapChat;
    public float doKho;
//    @ManyToOne
//    public LoaiHang loaiHang;
//    public int kieuSP;
    public Date thoiGian;
    @ManyToOne
    public User user;
    @ManyToOne
    public CaiDat chiNhanh;

    public boolean sync;
    public int edit;

    @Transient
    private SimpleStringProperty cl_soPhieu;
    @Transient
    private SimpleStringProperty cl_tongTL;
    @Transient
    private SimpleStringProperty cl_tlMun;
    @Transient
    private SimpleStringProperty cl_tlQuaCo;
    @Transient
    private SimpleStringProperty cl_tlVo;
    @Transient
    private SimpleStringProperty cl_tlTapChat;
    @Transient
    private SimpleStringProperty cl_tyLeMun;
    @Transient
    private SimpleStringProperty cl_tyLeQuaCo;
    @Transient
    private SimpleStringProperty cl_tyLeVo;
    @Transient
    private SimpleStringProperty cl_tyLeTapChat;
    @Transient
    private SimpleStringProperty cl_nhanVien;
    @Transient
    private SimpleStringProperty cl_thoiGian;
    @Transient
    private SimpleStringProperty cl_doKho;


    public void updateProperty() {
        cl_soPhieu = new SimpleStringProperty(soPhieu + "");
        cl_tongTL = new SimpleStringProperty(tongTL + "");
        cl_tlMun = new SimpleStringProperty(tlMun + "");
        cl_tlQuaCo = new SimpleStringProperty(tlQuaCo + "");
        cl_tlVo = new SimpleStringProperty(tlVo + "");
        cl_tlTapChat = new SimpleStringProperty(tlTapChat + "");
        cl_tyLeMun = new SimpleStringProperty(tyLeMun + "");
        cl_tyLeQuaCo = new SimpleStringProperty(tyLeQuaCo + "");
        cl_tyLeVo = new SimpleStringProperty(tyLeVo + "");
        cl_tyLeTapChat = new SimpleStringProperty(tyLeTapChat + "");
        cl_doKho = new SimpleStringProperty(doKho + "");
//        cl_loaiHang = new SimpleStringProperty(loaiHang != null ? loaiHang.tenLoai : "");
//        cl_kieuSP = new SimpleStringProperty(KieuSanPham.toString(kieuSP));
        cl_nhanVien = new SimpleStringProperty(user.fullname == null ? "" : user.fullname);
        cl_thoiGian = new SimpleStringProperty(thoiGian == null ? "" : new SimpleDateFormat("dd/MM/yyyy").format(thoiGian));

    }

  

    public String getCl_soPhieu() {
        return cl_soPhieu.getValue();
    }

    public String getCl_tongTL() {
        return cl_tongTL.getValue();
    }

    public String getCl_tlMun() {
        return cl_tlMun.getValue();
    }

    public String getCl_tlQuaCo() {
        return cl_tlQuaCo.getValue();
    }

    public String getCl_tlVo() {
        return cl_tlVo.getValue();
    }

    public String getCl_tlTapChat() {
        return cl_tlTapChat.getValue();
    }

    public String getCl_tyLeMun() {
        return cl_tyLeMun.getValue();
    }

    public String getCl_tyLeQuaCo() {
        return cl_tyLeQuaCo.getValue();
    }

    public String getCl_tyLeVo() {
        return cl_tyLeVo.getValue();
    }

    public String getCl_tyLeTapChat() {
        return cl_tyLeTapChat.getValue();
    }

    public String getCl_nhanVien() {
        return cl_nhanVien.getValue();
    }

    public String getCl_thoiGian() {
        return cl_thoiGian.getValue();
    }

    public String getCl_doKho() {
        return cl_doKho.getValue();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PhieuKCS)) {
            return false;
        }
        PhieuKCS other = (PhieuKCS) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PhieuKCS[ id=" + id + " ]";
    }

}
