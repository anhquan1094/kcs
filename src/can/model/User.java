/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.model;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * @author maxcd
 */
@Entity
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    private String username;
    private String password;
    private Long idChiNhanh;
    private int role;
    public String session;
    public String fullname;
    public boolean available;

    @Transient
    private SimpleStringProperty cl_id;
    @Transient
    private SimpleStringProperty cl_username;
    @Transient
    private SimpleStringProperty cl_role;

    public void updateProperty() {
        cl_id = new SimpleStringProperty(id + "");
        cl_username = new SimpleStringProperty(username + "");
        if (role == 1) {
            cl_role = new SimpleStringProperty("Root");
        } else if (role == 2) {
            cl_role = new SimpleStringProperty("Quản lý");
        } else {
            cl_role = new SimpleStringProperty("Nhân viên");
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getIdChiNhanh() {
        return idChiNhanh;
    }

    public void setIdChiNhanh(Long idChiNhanh) {
        this.idChiNhanh = idChiNhanh;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getCl_id() {
        return cl_id.getValue();
    }

    public String getCl_username() {
        return cl_username.getValue();
    }

    public String getCl_role() {
        return cl_role.getValue();
    }

    public void setCl_id(SimpleStringProperty cl_id) {
        this.cl_id = cl_id;
    }

    public void setCl_username(SimpleStringProperty cl_username) {
        this.cl_username = cl_username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "can.model.User[ id=" + id + " ]";
    }

}
