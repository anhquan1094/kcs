/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can;

import can.dao.UserJpaController;
import can.model.User;
import can.util.CheckInternet;
import can.util.Dialog;
import can.util.State;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author VMT
 */
public class Can extends Application {

    public static Stage primaryStage;

    @Override
    public void start(Stage stage) throws Exception {

        initData();
        System.out.println("Start");
        primaryStage = stage;
        stage.setTitle("Login");

        Parent root = FXMLLoader.load(getClass().getResource("view/Login.fxml"));
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });

    }

    private void initData() {

        try {
            Connection connection
                    = DriverManager.getConnection("jdbc:mysql://localhost/", "root", "1234");
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + "kcs"+ " DEFAULT CHARACTER SET utf8;");
            stmt.close();
            connection.close();
            EntityManager em = State.getEm();
        } catch (SQLException ex) {
            Logger.getLogger(Can.class.getName()).log(Level.SEVERE, null, ex);
            Dialog.showError("Không thể kết nối cơ sở dữ liệu!");
            System.exit(1);
            return;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
