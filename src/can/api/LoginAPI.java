/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.api;

import can.dao.UserJpaController;
import can.model.User;
import can.util.State;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author maxcd
 */
public class LoginAPI {

    public static boolean checkLogin(String username, String password) {
        try {
            String body = Jsoup.connect(State.getCaiDat().host + "/api/request/login")
                    .ignoreContentType(true)
                    .data("username", username)
                    .data("password", password)
                    .post().body().text();
            Gson gson = new Gson();
            JsonParser jsonParser = new JsonParser();
            JsonObject map = (JsonObject) jsonParser.parse(body);

            String message = map.get("message").getAsString();
            System.out.println(message);
            if (!"success".equals(message)) {
                return false;
            }
            JsonObject data = map.get("data").getAsJsonObject();
            Long user_id = data.get("id").getAsLong();

            UserJpaController userJpaController = new UserJpaController(State.getEmf());

            User user = userJpaController.findUser(user_id);
            if (user == null) {
                user = new User();
                user.setRole(3);
            }

            user.setId(user_id);
            user.setUsername(username);
            user.setPassword(password);
            //if idchii nhanh ==? return true; else false
            //  user.setIdChiNhanh(1l);
            user.available = true;
            user.fullname = data.get("email").getAsString();
//                //
            userJpaController.edit(user);
            State.setUser(user);

        } catch (Exception ex) {
            ex.printStackTrace();
//            Logger.getLogger(LoginAPI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

}
