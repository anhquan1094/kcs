/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.api;

import can.dao.UserJpaController;
import can.model.User;
import can.util.State;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;

/**
 *
 * @author maxcd
 */
public class DoiMatKhauAPI {

    public static boolean excute(String oldPass, String newPass) {
        try {
            User user = State.getUser();
            String body = Jsoup.connect(State.getCaiDat().host+"/api/request/change-pass")
                    .ignoreContentType(true)
                    .data("user_name", user.getUsername())
                    .data("old_password", oldPass)
                    .data("new_password", newPass)
                    .post().body().text();

            Gson gson = new Gson();
            Map<String, Object> map = gson.fromJson(body, Map.class);
            String message = map.get("message").toString();
            System.out.println(message);
            if (!message.equals("Cập nhật mật khẩu thành công")) {
                return false;
            }
            user.setPassword(newPass);
            State.setUser(user);
            new UserJpaController(State.getEmf()).edit(user);

        } catch (Exception ex) {
            Logger.getLogger(DoiMatKhauAPI.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
