/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.api;

import can.dao.LogJpaController;
import can.dao.PhieuKCSJpaController;
import can.model.Log;
import can.model.PhieuKCS;
import com.google.gson.Gson;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;

/**
 *
 * @author maxcd
 */
public class PushPhieuKCS extends Thread {

    @Override
    public void run() {
        synPhieuCanThuong();
    }

    private void synPhieuCanThuong() {
        String sql = "";
        if (can.util.State.getUser().getRole() == 2) {
            sql = "SELECT * FROM PhieuKCS where sync=0;";
        } else if (can.util.State.getUser().getRole() == 3) {
            sql = "SELECT * FROM PhieuKCS where sync=0 and user_id=" + can.util.State.getUser().getId() + ";";
        } else {
            return;
        }
        List<PhieuKCS> phieuCans = can.util.State.getEm().createNativeQuery(sql, PhieuKCS.class).getResultList();
        for (PhieuKCS pc : phieuCans) {
            try {
                String thoiGian = pc.thoiGian == null ? "" : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(pc.thoiGian);

                String body = Jsoup.connect(can.util.State.getCaiDat().host + "/api/request/log-scale")
                        .ignoreContentType(true)
                        .data("type", "KCS")
                        .data("id_user", pc.user.getId() + "")
                        .data("soPhieu", pc.soPhieu)
                        .data("tongTL", pc.tongTL + "")
                        .data("tlMun", pc.tlMun + "")
                        .data("tlQuaCo", pc.tlQuaCo + "")
                        .data("tlVo", pc.tlVo + "")
                        .data("tlTapChat", pc.tlTapChat + "")
                        .data("tyLeMun", pc.tyLeMun + "")
                        .data("tyLeQuaCo", pc.tyLeQuaCo + "")
                        .data("tyLeTapChat", pc.tyLeTapChat + "")
                        .data("tyLeVo", pc.tyLeVo + "")
                        .data("doKho", pc.doKho + "")
                        .data("thoiGian", thoiGian + "")
                        .data("code", can.util.State.getCaiDat().maChiNhanh + "")
                        .data("app_id", can.util.State.getCaiDat().keyApp)
                        .post().body().text();
                System.out.println(body);
//                Gson gson = new Gson();
                PhieuKCSJpaController pcjc = new PhieuKCSJpaController(can.util.State.getEmf());
                pc.sync = true;
                pcjc.edit(pc);

                Log log = new Log();
                log.content = pc.getId() + " - " + body;
                log.time = new Date();
                log.creater = can.util.State.getUser().getId();
                new LogJpaController(can.util.State.getEmf()).create(log);
            } catch (Exception ex) {
                Logger.getLogger(PushPhieuKCS.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    Log log = new Log();
                    log.content = "Không thể đồng bộ phiếu kcs: " + pc.getId();
                    log.time = new Date();
                    log.creater = can.util.State.getUser().getId();
                    new LogJpaController(can.util.State.getEmf()).create(log);
                } catch (Exception ex1) {
                }
            }
        }
    }
}
