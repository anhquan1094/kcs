/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package can.api;

import can.dao.CaiDatJpaController;
import can.model.CaiDat;
import can.util.State;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;

/**
 *
 * @author maxcd
 */
public class CaiDatAPI {

    public static String update(String keyApp, String maChiNhanh) {
        try {
            String body = Jsoup.connect(State.getCaiDat().host + "/api/install/kcs/" + keyApp + "/" + maChiNhanh )
                    .ignoreContentType(true)
                    .post().body().text();
//            Gson gson = new Gson();
            JsonParser jsonParser = new JsonParser();
            JsonObject map = (JsonObject) jsonParser.parse(body);

            String message = map.get("message").getAsString();
            if (!"success".equals(message)) {
                return message;
            }
            JsonObject data = map.get("data").getAsJsonObject();
            JsonObject company = data.get("company").getAsJsonObject();
            JsonObject department = data.get("department").getAsJsonObject();

            

            CaiDatJpaController chiNhanhJpaController = new CaiDatJpaController(State.getEmf());
            CaiDat caiDat = chiNhanhJpaController.findCaiDat(1L);
            if (caiDat == null) {
                caiDat = new CaiDat();
            }
            caiDat.tenCty = company.get("name").getAsString();
            caiDat.idCty = company.get("id").getAsString();
            //  chiNhanh.diaChiCty = "Tổ 2-Khu 6C -p.Hồng hải -TP.Hạ Long -Quảng Ning";

            caiDat.maChiNhanh = department.get("code").getAsString();
            caiDat.idChiNhanh = department.get("id").getAsString();
            caiDat.tenChiNhanh = department.get("name").getAsString();
            caiDat.keyApp = keyApp;

            chiNhanhJpaController.edit(caiDat);
            State.setCaiDat(caiDat);

        } catch (Exception ex) {
            Logger.getLogger(CaiDatAPI.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
        return "success";
    }
}
